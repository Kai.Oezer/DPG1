//  Copyright 2021 Kai Oezer, David Williames

import Foundation
import CoreBluetooth

/**
 Abstraction of the Bluetooth communication protocol for the Linak DPG1C control unit.

 Based on information from
 https://tyers.io/posts/14-idasen-controller/
 https://npm.io/package/idasen-controller
 https://github.com/j5lien/esphome-idasen-desk-controller
*/
struct BLEInterface
{
	static let motionControlService          = CBUUID(string:"99FA0001-338A-1024-8A49-009C0215F78A")
	static let motionControlCharacteristic   = CBUUID(string:"99FA0002-338A-1024-8A49-009C0215F78A")
	static let motionControlCharacteristic2  = CBUUID(string:"99FA0003-338A-1024-8A49-009C0215F78A")

	static let positionService               = CBUUID(string:"99FA0020-338A-1024-8A49-009C0215F78A")
	static let positionCharacteristic        = CBUUID(string:"99FA0021-338A-1024-8A49-009C0215F78A")
	static let positionCharacteristic2       = CBUUID(string:"99FA0029-338A-1024-8A49-009C0215F78A")
	static let positionCharacteristic3       = CBUUID(string:"99FA002A-338A-1024-8A49-009C0215F78A")

	// This service is for moving the desk actuator towards a given absolute position.
	// Allowed positions are 0 to 650 millimeters.
	// Just like the motion control commands, the position needs to be sent as a byte-reversed
	// 16-bit integer, after multiplying the millimeter position with 10.
	// That is, to move the desk to 65 cm (= 650 mm = 6500 Linak metric = 1964 hex) we send 0x6419.
	// This value needs to be sent repeatedly in approximately half-second intervals.
	// This control mode is only enabled after a message was sent to 99FA0002-...
	static let positionControlService        = CBUUID(string:"99fa0030-338a-1024-8a49-009c0215f78a")
	static let positionControlCharacteristic = CBUUID(string:"99fa0031-338a-1024-8a49-009c0215f78a")


	struct CommandDescriptor
	{
		let data : Data
		let characteristicID : CBUUID
		let writeType : CBCharacteristicWriteType
	}

	static let commands : [DPG1CommandType : CommandDescriptor] = Dictionary(uniqueKeysWithValues:
		(
			[
				(.moveDown , motionControlCharacteristic   , .withResponse),
				(.moveUp   , motionControlCharacteristic   , .withResponse),
				(.stop     , motionControlCharacteristic   , .withResponse),
				(.prepMove , motionControlCharacteristic   , .withoutResponse),
				(.moveTo   , positionControlCharacteristic , .withoutResponse),
			]
			as [(DPG1CommandType, CBUUID, CBCharacteristicWriteType)]
		)
		.map { ($0.0, CommandDescriptor(data:$0.0.characteristicValue, characteristicID: $0.1, writeType: $0.2)) }
	)

	static func update(_ state : inout DPG1State, from characteristic : CBCharacteristic) -> Bool
	{
		if characteristic.uuid == positionCharacteristic
		{
			guard let value = characteristic.value else { return false }
			state.position = _extractPosition(from: value)
			return true
		}
		return false
	}

	/// Extracts position value from the given characteristic data.
	///
	/// The returned position is the sum of the actuator extension
	/// and the height of the desk at its lowest position.
	///
	/// Based on [code from David Williames](https://github.com/DWilliames/idasen-controller-mac/blob/v1.0/Desk%20Controller/Desk%20control/DeskPeripheral.swift).
	private static func _extractPosition(from data : Data) -> DPG1State.Position
	{
		guard data.count > 1 else { return DPG1State.invalidPosition }

		// Position = 16 Little Endian – Unsigned
		// Speed = 16 Little Endian – Signed
		let uint16Value = [data[0], data[1]].withUnsafeBytes {
			$0.load(as: UInt16.self)
		}
		assert(uint16Value <= 6500, "the Idasen legs are expected to extend only up to 65 cm")
		let position = DPG1State.Position(uint16Value)

	#if false && DEBUG
		if data.count > 3
		{
			let speedValue = [value[2], value[3]].withUnsafeBytes {
				$0.load(as: Int16.self)
			}
			debugPrint("desk speed : \(speedValue)")
		}
	#endif

		return position
	}

}
