//  Copyright 2021 Kai Oezer

import Foundation

/// Represents a LINAK DPG instance for use in SwiftUI.
public class DPG1 : ObservableObject
{
	@Published public var state = DPG1State()

	public init() {}
}

public struct DPG1State
{
	/// extension of the actuator measured in a tenth of a millimeter (= 100 micrometers)
	public typealias Position = UInt16

	public static let invalidPosition = Position.max

	public struct PresetPositions : Equatable
	{
		public let position1 : Position
		public let position2 : Position
		public let position3 : Position

		public static let none = PresetPositions(position1: invalidPosition, position2: invalidPosition, position3: invalidPosition)
	}

	public var id : UUID
	public var name : String

	public var position : Position

	public var ownerPresets : PresetPositions
	public var guestPresets : PresetPositions

	public var connected : Bool

	public init(id : UUID = UUID(),
		name : String = "",
		position : Position = invalidPosition,
		ownerPresets : PresetPositions = .none,
		guestPresets : PresetPositions = .none,
		connected : Bool = false)
	{
		self.id = id
		self.name = name
		self.position = position
		self.ownerPresets = ownerPresets
		self.guestPresets = guestPresets
		self.connected = connected
	}

	mutating func reset()
	{
		ownerPresets = .none
		guestPresets = .none
		connected = false
	}
}
