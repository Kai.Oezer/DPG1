// Copyright 2021 Kai Oezer

import Foundation
import Combine
import CoreBluetooth

/// Represents the wireless connection to a nearby Idasen desk.
public class DPG1Connection : NSObject
{
	public enum ConnectionState
	{
		case unknown
		case poweredOff
		case error(String)
		case searching
		case connected

		init(cbState : CBManagerState)
		{
			switch cbState
			{
				case .poweredOff:  self = .poweredOff
				case .poweredOn:   self = .searching
				case .resetting:   self = .poweredOff
				case .unauthorized: self = .error("This app has no permission to access Bluetooth communication.")
				case .unsupported: self = .error("Bluetooth not supported")
				default: self = .unknown
			}
		}
	}

	/// provides SwiftUI views with updates to the captured remote desk state
	public let remoteStatePublisher = PassthroughSubject<DPG1State, Never>()

	public let connectionStatePublisher = PassthroughSubject<ConnectionState, Never>()

	/// Represents the state of the currently connected remote desk.
	///
	/// The state is updated as values are received from the peripheral.
	private var _remoteDeskState = DPG1State()

	private var _connectionState : ConnectionState = .poweredOff
	{
		didSet { connectionStatePublisher.send(_connectionState) }
	}

	private var _centralManager: CBCentralManager?

	private let _peripheralNamePattern : String
	private var _connectedPeripheral: CBPeripheral? // Or is currently being connecting to
	private var _connectionPending = false

	private var _services : [CBUUID : CBService] = [:]
	private var _characteristics : [CBUUID : CBCharacteristic] = [:]

	/// - Parameter pheriperalNamePattern: The regular expression used when searching the DPG1 peripheral by matching name.
	public init(searchPattern : String = #"^\Desk [0-9]{1,}$"#)
	{
		_peripheralNamePattern = searchPattern
		super.init()
		scanDevices()
	}

	/// (re-)scans for DPG1 peripherals
	public func scanDevices()
	{
		if let peripheral = _connectedPeripheral
		{
			_centralManager?.cancelPeripheralConnection(peripheral)
			_connectedPeripheral = nil
		}
		_centralManager = CBCentralManager(delegate: self, queue: DispatchQueue(label: "DPG1 BLE"))
	}

	/// (re-)scans services of the connected periperal
	public func scanDeviceServices()
	{
		_services.removeAll()
		_characteristics.removeAll()
		_connectedPeripheral?.discoverServices(nil)
	}

	/// Sends the given command with the optional data.
	/// - Parameters:
	///   - command: the command to send
	///   - value: the data for the command
	public func send(command : DPG1CommandType, value : Data? = nil)
	{
		guard let descriptor = BLEInterface.commands[command] else { fatalError("Sending an unknown command.") }
		guard let characteristic = _characteristics[descriptor.characteristicID] else { assert(false, "using an unregistered CBCharacteristic"); return }
		let dataToSend = value ?? descriptor.data
		_connectedPeripheral?.writeValue(dataToSend, for: characteristic, type: descriptor.writeType)
	}

	private func _publishRemoteState()
	{
		remoteStatePublisher.send(_remoteDeskState)
	}

	private func _updateConnectionState()
	{
		guard let manager = _centralManager else { _connectionState = .unknown; return }
		if manager.authorization == .denied
		{
			_connectionState = .error("Bluetooth access was denied. You may have to delete and re-install this application.")
		}
		else
		{
			_connectionState = ConnectionState(cbState: manager.state)
		}
	}
}

extension DPG1Connection : CBCentralManagerDelegate
{
	public func centralManagerDidUpdateState(_ central: CBCentralManager)
	{
		_centralManager = central

		_updateConnectionState()

		guard central.state == .poweredOn else { return }

		central.scanForPeripherals(withServices: nil, options: nil)
	}

	public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
	{
		guard _connectedPeripheral == nil, !_connectionPending,
			let name = peripheral.name, name.range(of: _peripheralNamePattern, options: .regularExpression) != nil,
			let connectable = advertisementData["kCBAdvDataIsConnectable"] as? Bool, connectable == true
			else { return }

		_connectionPending = true
		_connectedPeripheral = peripheral // it is required to hold on to the given CBPeripheral object with a strong link
		central.connect(peripheral, options: nil)
	}

	public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral)
	{
		guard _connectionPending, _connectedPeripheral === peripheral else { return }
		_connectionPending = false
		_connectedPeripheral = peripheral
		central.stopScan()
		_remoteDeskState.name = peripheral.name ?? "<unnamed>"
		_remoteDeskState.connected = true
		_publishRemoteState()
		peripheral.delegate = self
		scanDeviceServices()
	}

	public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?)
	{
		guard !_connectionPending, _connectedPeripheral == peripheral else { return }
		_connectedPeripheral = nil
		_connectionPending = false
		_remoteDeskState.reset()
		_publishRemoteState()
		central.scanForPeripherals(withServices: nil, options: nil)
	}
}

extension DPG1Connection : CBPeripheralDelegate
{
	public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?)
	{
		guard peripheral == _connectedPeripheral,
			let services = peripheral.services
			else { return }

		services.forEach { service in
			if (service.uuid == BLEInterface.positionService)
				|| (service.uuid == BLEInterface.motionControlService)
			{
				_services[service.uuid] = service
			}

			peripheral.discoverCharacteristics(nil, for: service)
		}
	}

	public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?)
	{
		guard peripheral == _connectedPeripheral,
			let characteristics = service.characteristics
			else { return }

		characteristics.forEach { characteristic in
			_characteristics[characteristic.uuid] = characteristic
			if characteristic.uuid == BLEInterface.positionCharacteristic
			{
				peripheral.readValue(for: characteristic)
				peripheral.setNotifyValue(true, for: characteristic)
			}
			else
			{
			#if false && DEBUG
				debugPrint("Discovered unused characteristic \(characteristic.uuid):")
				if let value = characteristic.value
				{
					debugPrint("  value: \(String(describing:value))")
				}
				if let descriptors = characteristic.descriptors
				{
					debugPrint("  descriptor values: \(descriptors.map{String(describing:$0.value)}.joined(separator:", "))")
				}
				debugPrint("  properties: \(String(describing:characteristic.properties))")
			#endif
			}
		}
	}

	public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?)
	{
		if BLEInterface.update(&_remoteDeskState, from: characteristic)
		{
			_publishRemoteState()
		}
	}
}
