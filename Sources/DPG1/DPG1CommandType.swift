// Copyright 2021 Kai Oezer

import Foundation

public enum DPG1CommandType : UInt8
{
	case moveDown  = 70  // 0x46
	case moveUp    = 71  // 0x47
	case ref2Down  = 72  // 0x48, not functional on Idasen DPG1
	case stop      = 255 // 0xFF
	case moveTo    = 254 // 0xFE, meta-command for characteristic 99fa0031
	case prepMove  = 0   // 0x00, meta-command to prepare for moveTo (see https://npm.io/package/idasen-controller)

	/// the command as two-byte data in network byte order
	/// (= big-endian = most significant bytes first)
	var characteristicValue : Data
	{
		var data = Data(capacity: 2)
		data.append(self.rawValue)
		data.append(0)
		return data
	}
}
