# DPG1

## Description

This library provides partial access to the functions of the LINAK DPG1 desk control unit via Bluetooth.   

## Motivation

* __Providing an open-source alternative, in case the official Linak app becomes unavailable.__  
IKEA Idåsen desks require the LINAK [Desk Control app](https://www.linak.com/products/controls/desk-control-app)
to set the preset positions. There is no guarantee that the official LINAK app will always be available
throughout the lifetime of your desk. An open-source alternative that replicates the functions of the
LINAK app is therefore needed. 

* __Enable the creation of custom control apps.__  
Owners of the IKEA Idåsen desk may need features which the LINAK app misses or would like to
be able to control the desk from devices which the LINAK app does not support.

## Features

* read current position
* move up/down
* move to a specific position

## Programming Interface

For an example of how to use this library have a look at my [Idasen](https://gitlab.com/idasen-control/idasen) project.

## Note on BLE peripheral search strategy

This DPG1 library was created for connecting to the DPG1 desk control panel of the IKEA Idåsen desk.
This panel does not advertise its main GATT service ID (_99FA0001-338A-1024-8A49-009C0215F78A_) or
any other model-specific information in its GAP broadcasts. The strategy for quickly searching for
an Idåsen DPG1 panel without connecting to all discovered devices is to match the name of the peripheral
to the default naming scheme "Desk N", where N is a positive integer number.

## Notes on the BLE communication protocol

### GATT Services

The Idåsen DPG1 offers the GATT services listed below

* __motion__  
  Service-ID: __99FA0001-338A-1024-8A49-009C0215F78A__

  Characteristics:
	- _99FA0002-338A-1024-8A49-009C0215F78A_
  		- properties (access modes): write, writeWithoutResponse
  		- values:  
					__move up__ : send 0x4700 (2 bytes)  
					__move down__ : send 0x4600 (2 bytes)  
					__stop__ : send 0xFF00 (2 bytes)  

	- _99FA0003-338A-1024-8A49-009C0215F78A_
  		- properties (access modes): read, notify
  		- values: none

* __owner/guest preset management????__  
  Service-ID: __99FA0010-338A-1024-8A49-009C0215F78A__

  Characteristics:
	- _99FA0011-338A-1024-8A49-009C0215F78A_
  		- descriptors:
  		- properties (access modes): read, write, writeWithoutResponse, notify

* __position / height__  
  Service-ID: __99FA0020-338A-1024-8A49-009C0215F78A__

  Characteristics:
	- _99FA0021-338A-1024-8A49-009C0215F78A_
  		- descriptors: 
  		- properties (access modes): read, notify
	- _99FA0029-338A-1024-8A49-009C0215F78A_
  		- descriptors: 
  		- properties (access modes): read
	- _99FA002A-338A-1024-8A49-009C0215F78A_
  		- descriptors:
  		- properties (access modes): read 

* __owner/guest preset management????__  
  Service-ID: __99FA0030-338A-1024-8A49-009C0215F78A__

  Characteristics:
	- _99FA0031-338A-1024-8A49-009C0215F78A_
  		- descriptors: none
  		- properties (access modes): write, writeWithoutResponse
